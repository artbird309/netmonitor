# netmonitor
A Bash script to check if the internet is still up and running, if it is not then reboot the modem using SNMP for APC PDU and send a pushover notification.

___
Based off bmatherly APC PDU Monitor Instructions: https://gist.github.com/bmatherly/3890485

___
I am running on Ubuntu 16.04 VM following this site to install SNMP https://tobinsramblings.wordpress.com/2011/05/03/snmp-tutorial-apc-pdus/

___
##Installing SNMP:
Install SNMP

    sudo apt-get install snmp

Install MIBs

    sudo apt-get install libsnmp-base

    sudo apt-get install snmp-mibs-downloader

Edit /etc/snmp/snmp.conf and comment out the line starting with 'mibs :'

    sudo nano /etc/snmp/snmp.conf

###Setup MIB for current User to run netmonitor.sh at user level:
We still need to install the APC vendor specific MIB. You can grab a copy of the MIB’s on APC’s download site or use the one I have provided.

    cp PowerNet-MIB.mib .snmp/mibs/PowerNet-MIB.mib

In order to active the mib, grep for DEFINITION:

    # grep DEFINITION .snmp/mibs/PowerNet-MIB.mib
        PowerNet-MIB DEFINITIONS ::= BEGIN

We’re almost done, next activate the Definition “PowerNet-MIB” by adding the following line to .snmp/snmp.conf

    cp snmp.conf .snmp/snmp.conf

Now SNMP is installed and setup with the APC specific MIB, you are ready to run the netmonitor.sh at the user level

###Setup MIB for Root to run netmonitor.sh at root level with cron:
We still need to install the APC vendor specific MIB. You can grab a copy of the MIB’s on APC’s download site or use the one I have provided.

    sudo cp PowerNet-MIB.mib /root/.snmp/mibs/PowerNet-MIB.mib

In order to active the mib, grep for DEFINITION:

    # sudo grep DEFINITION /root/.snmp/mibs/PowerNet-MIB.mib
        PowerNet-MIB DEFINITIONS ::= BEGIN

We’re almost done, next activate the Definition “PowerNet-MIB” by adding the following line to .snmp/snmp.conf or copy from my files

    sudo cp snmp.conf /root/.snmp/snmp.conf

Now SNMP is installed and setup with the APC specific MIB, you are ready to run the netmonitor.sh at the root level and can run with cron.

###Setup netmonitor.sh to run from root cron:
Need to update the variables at the top of the script to your own values to get the script working.

    nano netmonitor.sh

Copy netmonitor.sh to bin directory

    sudo cp netmonitor.sh /usr/bin/netmonitor.sh

Change permissions to all root full access

    sudo chmod 755 /usr/bin/netmonitor.sh

Open root crontab to add netmonitor.sh to run by itself.

    sudo crontab -e

Add to crontab and run every 10 minutes and log errors to syslog:

    # Runs netmonitor.sh every 10 minutes to check internet and reboot modem if needed.
    */10 * * * * /usr/bin/netmonitor.sh 2>&1 | /usr/bin/logger -t netmonitor
