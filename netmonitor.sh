#!/bin/bash
####################################
#
# Check if the internet is still up and running, if it is not then reboot the modem and send a pushover notification.
#
# Timeline: Runs first ping test, if this fails it sleeps 60s then tries the second host if that fails it reboots the modem port then sleeps 90s
#   before sending the Pushover notification. If the notification fails to send it sleeps 60s then tries again, if that fails it exits.
#   Longest running time will be if everything fails, 210s or 3.5m plus the ping time which I have seen to be between 2-20s depending on if it timesout or not.
#
# Need to change variables to your own values to get the script working
#
####################################

declare -r SWITCH_ADDRESS="IP address to the APC PDU"
declare -r BASE_SWITCH_OID="PowerNet-MIB::sPDUOutletCtl"
declare -r CABLE_MODEM_OUTLET="Port the cable modem is connect to and will be rebooted"
declare -r PRIMARY_NET_TEST_HOST="www.google.com"
declare -r SECODARY_NET_TEST_HOST="www.yahoo.com"
declare -r SNMP_WRITE_ACCESS="Use your own SNMP Write+ acess string"
declare -r PUSHOVER_ENABLE="0=disbabled, 1= enabled"
declare -r PUSHOVER_APP_TOKEN="Use your own app key here"
declare -r PUSHOVER_USER_TOKEN="Use your own user key here"
declare -r TELEGRAM_ENABLE="0=disbabled, 1= enabled"
declare -r TELEGRAM_CHAT_ID="Use your own chat ID here which you can get from botID"
declare -r TELEGRAM_BOT_TOKEN="Use your own bot token here which you can get from botfather"
declare -r NOTIFICATION_DEBUG="0=disbabled, 1= enabled"


#
# Below this line is the script which you should not need to edit to get it working.
#####################################
#

# Define variables that should not change, the Telegram bot url to send a message and a newline variable
declare -r TELEGRAM_BOT_URL="https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage"
declare -r NEWLINE=$'\n'

# Define a timestamp function to output in Unix Time
timestamp()
{
  date +"%s"
}

# Define a formatted timestamp fucntion to output in "03 Oct 2016 20:20"
formatted_timestamp()
(
  date "+%d %b %Y %H:%M"
)

# Define the function to turn on a port passing it the port numnber
power_on_device()
{
   echo "Powering on device $1"
   snmpset -v 1 -c $SNMP_WRITE_ACCESS $SWITCH_ADDRESS $BASE_SWITCH_OID.$1 i 1 >> /dev/null
}

# Define the function to turn off a port passing it the port numnber
power_off_device()
{
   echo "Powering off device $1"
   snmpset -v 1 -c $SNMP_WRITE_ACCESS $SWITCH_ADDRESS $BASE_SWITCH_OID.$1 i 2 >> /dev/null
}

# Define the function to reboot port passing it the port numnber
reboot_device()
{
   echo "Rebooting device $1"
   snmpset -v 1 -c $SNMP_WRITE_ACCESS $SWITCH_ADDRESS $BASE_SWITCH_OID.$1 i 3 >> /dev/null
}

# Define the function to use curl to send a notification using Pushover if curl retuns something besides a 0 (good) it will wait and try again, if that fails it exits.
# Needed variables passed to it, $1="String Title of notification", $2="interger unix timestamp", $3="String message to be sent"
send_pushover()
{
  curl -s \
    --form-string "token=$PUSHOVER_APP_TOKEN" \
    --form-string "user=$PUSHOVER_USER_TOKEN" \
    --form-string "title=$1" \
    --form-string "timestamp=$2" \
    --form-string "message=$3" \
    https://api.pushover.net/1/messages.json
    if [ $? -ne 0 ]; then
      echo -e "\nPushover failed waiting 60s then trying again"
      sleep 60s
      curl -s \
        --form-string "token=$PUSHOVER_APP_TOKEN" \
        --form-string "user=$PUSHOVER_USER_TOKEN" \
        --form-string "title=$1" \
        --form-string "timestamp=$2" \
        --form-string "message=$3" \
        https://api.pushover.net/1/messages.json
        if [ $? -ne 0 ]; then
          echo -e "\nPushover failed exiting"
        else
          echo -e "\nPushover Sent on second try"
        fi
    else
      echo -e "\nPushover Sent"
    fi
}

# Define the function to use curl to send a notification using Telegram if curl retuns something besides a 0 (good) it will wait and try again, if that fails it exits.
# Needed variables passed to it, $1="String message to be sent with intergated title"
send_telegram()
{
  curl -s -d "chat_id=$TELEGRAM_CHAT_ID&parse_mode=Markdown&text=$1" $TELEGRAM_BOT_URL
    if [ $? -ne 0 ]; then
      echo -e "\nTelegram failed waiting 60s then trying again"
      sleep 60s
      curl -s -d "chat_id=$TELEGRAM_CHAT_ID&parse_mode=Markdown&text=$1" $TELEGRAM_BOT_URL
        if [ $? -ne 0 ]; then
          echo -e "\nTelegram failed exiting"
        else
          echo -e "\nTelegram Sent on second try"
        fi
    else
      echo -e "\nTelegram Sent"
    fi
}

# Define the function to test the internet by pinging and if it returns a 0 it exits else it trys a differnt host, if that fails it returns a 0 else a 1
test_internet()
{
   ping -c 4 -q $PRIMARY_NET_TEST_HOST >> /dev/null
   if [ $? -ne 0 ]; then
      echo "Primary internet host ping failure waiting 60s then trying Secondary"
      sleep 60s
      ping -c 4 -W 10 -q $SECODARY_NET_TEST_HOST >> /dev/null
      if [ $? -ne 0 ]; then
         echo "Secondary internet host ping failure"
         return 0
      fi
   fi
   return 1
}

# Call the main function on startup to test the internet and see if it is still up
test_internet
if [ $? -eq 0 ]; then
   reboot_device $CABLE_MODEM_OUTLET
   echo -e "Waiting 90s then trying to send notification"
   sleep 90s
   # Send Pushover internet died notification
   if [ $PUSHOVER_ENABLE = 1 ]; then
      send_pushover "Internet Rebooted" $(timestamp) "I guess the internet died and I have rebooted the modem."
   fi
   # Send Telegram internet died notification
   if [ $TELEGRAM_ENABLE = 1 ]; then
      send_telegram "*Internet Rebooted*$NEWLINE I guess the internet died and I have rebooted the modem. $(formatted_timestamp)"
   fi
else
  echo "Internet is good!"
  # Send a debug notification if DEBUG is enabled that I ran and the internet is good notification
  if [ $NOTIFICATION_DEBUG = 1 ]; then
     send_pushover "Internt is Good" $(timestamp) "The Internet is good and I ran without errors."
  fi
  # Send Telegram internet died notification
  if [ $NOTIFICATION_DEBUG = 1 ]; then
     send_telegram "*Internt is Good*$NEWLINE The Internet is good and I ran without errors. $(formatted_timestamp)"
  fi
fi